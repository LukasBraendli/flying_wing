/* ============================================
I2Cdev device library code is placed under the MIT license
Copyright (c) 2012 Jeff Rowberg
=============================================== */

//#define DEBUG_OUTPUT


// ================================================================
// ===                         INCLUDE                          ===
// ================================================================
#include "I2Cdev.h"

#include "MPU6050_6Axis_MotionApps20.h"
//#include "MPU6050.h" // not necessary if using MotionApps include file

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// SPI is needed for the communication with the nRF24L01 module
#include <SPI.h>

#include <RF24.h>
#include <nRF24L01.h>

// Servo.h is used to communicate/send values to ESC's
#include <Servo.h>



// ================================================================
// ===               VARIABLES, CLASS OBJECTS...                ===
// ================================================================
// Pins for connection with nRF24L01
#define CE_Pin 7
#define CSN_Pin 8


// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for SparkFun breakout and InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 mpu;
// MPU6050 mpu(0x69); // <-- use for AD0 high

int16_t gyro_fullScaleRange = 500; // +/- 500 °/s
#define GYRO_FS MPU6050_GYRO_FS_500 // +/- 500 °/s

struct gyroXY_t{
  int16_t gx;           // angular speed around x-axis; value from -32768 to +32767
  int16_t gy;           // angular speed around y-axis; value from -32768 to +32767
  float gx_DEG;       // angular speed around x-axis in °/s
  float gy_DEG;       // angular speed around y-axis in °/s
  float pitch_rate;          // pitch rate in °/s
  float roll_rate;           // roll rate in °/s
  double pitch_avg_rate;
  double roll_avg_rate;
} gyroXY;

#define PITCH_OFFSET  2.0
#define ROLL_OFFSET   -0.6
#define GYRO_THRESHOLD    10 // under x°/s NO movement is registered

/* Gyro offsets:
 *  XGyro     YGyro     ZGyro
 *  [-1,3]    [-1,2]    [-2,1]

 */


// create new RF24 instance called radio with connected pins as arguments
RF24 radio(CE_Pin, CSN_Pin);

//create new Servo instances
Servo esc_front, esc_back, servo_right, servo_left;
#define ESC_FRONT_PIN 3
#define ESC_BACK_PIN 9
#define SERVO_RIGHT_PIN 5
#define SERVO_LEFT_PIN 6

#define SERVO_RIGHT_MID 90
#define SERVO_RIGHT_MIN 60
#define SERVO_RIGHT_MAX 150

#define SERVO_LEFT_MID  50
#define SERVO_LEFT_MIN  0
#define SERVO_LEFT_MAX  90

// nRF24L01
const byte rxAddr[6] = "00011"; // address; has to be the same on transmitter

uint16_t radio_failsave_delay = 300; // time until failsave kicks in on RC-signal-loss in milliseconds
bool failsave_happened = 0;     // is set to one in case of RC-signal-loss
unsigned long prev_time_radio = 0;
unsigned long current_time = 0;

struct data2{           //DATA TO BE RECIEVED
  uint16_t throttle;        // throttle value
  uint16_t yaw;             // yaw value
  uint16_t pitch;           // pitch value
  uint16_t roll;            // roll value
  bool b1;              // boolean varaible, i.e. for a button/switch
  bool b2;              // boolean variable, i.e. for a button/switch
  byte poti;
  byte PID_tuning;  // bits 7:6 select parameter --> 00 = NONE, 01 = P, 10 = I, 11 = D
                    // bits 5:1 parameter map upper bound
                    // bit 0 tuning enable --> 0 = DISABLED, 1 = ENABLED
} steeringData;

int16_t min_motor_val = 1120;
int16_t max_motor_val = 1864;

bool arming_state = 0;
uint16_t throttle = 0;
int16_t pitch_targetRate = 0;
int16_t roll_targetRate = 0;

int16_t pitch_maxRate = 200; // max °/s
int16_t roll_maxRate = 400;  // max °/s

int16_t pitch_trim = 100;  // negative --> nose down
int16_t roll_trim = 80;  // negative --> left roll

uint8_t aileron_right = (SERVO_RIGHT_MAX + SERVO_RIGHT_MIN) / 2;
uint8_t aileron_left = (SERVO_LEFT_MAX + SERVO_LEFT_MIN) / 2;


// ----- PD-control -----
float P = 10; // Default: 18 --> "+" = agil --> "-" = "Camera-setting"
float I = 5;
float D = 4; // Default: 25 --> "-" = agil --> "+" = "Camera-setting"
int pitch_err;
int roll_err;
float PD_val_pitch;
float PD_val_roll;

int16_t prev_err_pitch = 0; // previous pitch error (°/s)
int16_t prev_err_roll = 0;  // previous roll error (°/s)

unsigned long prev_time_pitch = 0;
unsigned long prev_time_roll = 0;

float pitch_err_integral; // 
float roll_err_integral;  // 

float pitch_err_derivative; // °/s²
float roll_err_derivative;  // °/s²

float delta_err = 0;
float delta_time = 0;

bool stabilizer_off = 0;  // stabilizer is off when this is true


// ----- time measurement -----
unsigned long last_time_meas = 0;


// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================
void setup() {
  #ifdef DEBUG_OUTPUT
    Serial.begin(115200);
  #endif
  
  //========== nRF24L01 SETUP ==========
  radio.begin();
  radio.openReadingPipe(0, rxAddr);
  radio.setDataRate(RF24_250KBPS);

  radio.enableAckPayload();
  radio.enableDynamicPayloads();
  
  radio.startListening();

  //========== initialize servos ==========
  esc_front.attach(ESC_FRONT_PIN);
  esc_back.attach(ESC_BACK_PIN);
  servo_right.attach(SERVO_RIGHT_PIN);
  servo_left.attach(SERVO_LEFT_PIN);

  delay(500);


  //========== GYRO SETUP ==========
  // join I2C bus (I2Cdev library doesn't do this automatically)
  #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
      Wire.begin();
      TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
  #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
      Fastwire::setup(400, true);
  #endif
  
  // initialize device
  #ifdef DEBUG_OUTPUT
    Serial.println(F("Initializing I2C devices..."));
  #endif
  mpu.initialize();

  // verify connection
  #ifdef DEBUG_OUTPUT
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));
  #endif

  mpu.setFullScaleGyroRange(GYRO_FS); // set full scale range

  #ifdef DEBUG_OUTPUT
    Serial.println("Setup complete");
  #endif

  // initialize stabilizer
  // nRF24L01 COMMUNICATION
  while(radio.available()){
    radio.read(&steeringData, sizeof(steeringData));
    prev_time_radio = millis();
  }

  if(steeringData.yaw < 200 && steeringData.roll > 800)
  {
    stabilizer_off = true;
  }
  
}



// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================
void loop() {
  //========== nRF24L01 COMMUNICATION ==========
  while(radio.available()){
    radio.read(&steeringData, sizeof(steeringData));
    prev_time_radio = millis();
  }

  //========== Failsave in case of signal loss ==========
  current_time = millis();
  if(current_time - prev_time_radio > radio_failsave_delay){
    failsave_happened = true;
  }
  else{
    failsave_happened = false;
  }

  arming_state = steeringData.b1;
  
  if(arming_state && (!failsave_happened)){
    // calculate THROTTLE from reciever-data
    throttle = map(steeringData.throttle, 0, 1023, min_motor_val, max_motor_val);
  }
  else{
    throttle = 1064;
  }

  pitch_targetRate = map(steeringData.pitch, 0, 1023, -pitch_maxRate, pitch_maxRate); // + pitch_trim;
  roll_targetRate = map(steeringData.roll, 0, 1023, -roll_maxRate, roll_maxRate);     // + roll_trim;


  //========== PROCESSING OF GYRO-DATA ==========
  int roll_counter = 0;
  int pitch_counter = 0;
  for(int i = 0; i < 10; i++)
  {
    gyroXY.gx = mpu.getRotationX();
    gyroXY.gy = mpu.getRotationY();
    
    gyroXY.gx_DEG = map(gyroXY.gx, -32768, 32767, -gyro_fullScaleRange, gyro_fullScaleRange);
    gyroXY.gy_DEG = map(gyroXY.gy, -32768, 32767, -gyro_fullScaleRange, gyro_fullScaleRange);

    if(gyroXY.gx_DEG < GYRO_THRESHOLD && gyroXY.gx_DEG > -GYRO_THRESHOLD)
    {
      gyroXY.roll_rate = 0;
    }
    else
    {
      gyroXY.roll_rate = -gyroXY.gx_DEG;
      roll_counter++;
    }

    if(gyroXY.gy_DEG < GYRO_THRESHOLD && gyroXY.gy_DEG > -GYRO_THRESHOLD)
    {
      gyroXY.pitch_rate = 0;
    }
    else
    {
      gyroXY.pitch_rate = gyroXY.gy_DEG;
      pitch_counter++;
    }
  
    gyroXY.pitch_avg_rate += gyroXY.pitch_rate;
    gyroXY.roll_avg_rate += gyroXY.roll_rate;
  }

  if(pitch_counter != 0 && !stabilizer_off)
  {
    gyroXY.pitch_avg_rate /= pitch_counter;
  }
  else
  {
    gyroXY.pitch_avg_rate = 0;
  }

  if(roll_counter != 0 && !stabilizer_off)
  {
    gyroXY.roll_avg_rate /= roll_counter;
  }
  else
  {
    gyroXY.roll_avg_rate = 0;
  }
  
  current_time = millis();
  pitch_err = pitch_targetRate - gyroXY.pitch_avg_rate;
  delta_err = float(pitch_err - prev_err_pitch);
  delta_time = (float(current_time - prev_time_pitch)) / 10;
  pitch_err_derivative = delta_err / delta_time;
  //pitch_err_integral += delta_err;
  prev_time_pitch = current_time;
  prev_err_pitch = pitch_err;
  
  current_time = millis();
  roll_err = roll_targetRate - gyroXY.roll_avg_rate;
  delta_err = float(roll_err - prev_err_roll);
  delta_time = (float(current_time - prev_time_roll)) / 10;
  roll_err_derivative = delta_err / delta_time;
  //roll_err_integral += delta_err;
  prev_time_roll = current_time;
  prev_err_roll = roll_err;

  //========== PD-CONTROL ==========  
  //---------- pitch ----------
  PD_val_pitch = (pitch_err * P - pitch_err_derivative * D) / 10;

  //---------- roll ----------
  PD_val_roll = (roll_err * P - roll_err_derivative * D) / 10;

  if(failsave_happened){
    PD_val_pitch = 0;
    PD_val_roll = 0;
    pitch_err_integral = 0;
    roll_err_integral = 0;
  }

  PD_val_pitch = constrain(PD_val_pitch + pitch_trim, -500, 500);
  PD_val_roll = constrain(PD_val_roll + roll_trim, -500, 500);

  aileron_right = constrain(map(-1*(PD_val_pitch+PD_val_roll), -500, 500, SERVO_RIGHT_MIN, SERVO_RIGHT_MAX), SERVO_RIGHT_MIN, SERVO_RIGHT_MAX);
  aileron_left = constrain(map((PD_val_pitch-PD_val_roll), -500, 500, SERVO_LEFT_MIN, SERVO_LEFT_MAX), SERVO_LEFT_MIN, SERVO_LEFT_MAX);
  
  
  servo_right.write(aileron_right);
  servo_left.write(aileron_left);
  esc_front.writeMicroseconds(throttle);
  esc_back.writeMicroseconds(throttle);
}
